## 1.0.0 (2020-09-03)

##### Features

* clean code tools: eslint, prettier, husky and lint-staged
* tools for standarized the commit messages: commitizen and cz-customizable
* it uses Markdown files as datasource
* pages are organized using the name of Markdown files
* internationalization (i18n) using the name of Markdown files
* it uses atomic design structure with a slightly change
* all the content can be modified from assets folder: text, images, icons, colors, borders, spacing, font, breakpoints and configuration data
* just changing a flag you can have a site with a single page or with multiple pages
* changelog generator and scripts for updating it and the version of the base template
* hooks: for detecting the page intercepting by the viewholder and for adding animation