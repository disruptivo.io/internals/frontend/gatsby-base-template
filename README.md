# 😕 What is this?

A base starter which was created using the [Gatsby's default starter](https://github.com/gatsbyjs/gatsby-starter-default), but it contains the following features:

- Tools for writing a clean code:
  - eslint
  - prettier
  - husky
  - lint-staged
- Tools for standarized the commit messages:
  - commitizen
  - cz-customizable
- It uses as datasource Markdown files
- All the content can be modified from assets folder:
  - texts
  - images
  - icons
  - colors
  - borders
  - spacing
  - font
  - breakpoints
  - configuration data
- It uses [atomic design structure](https://bradfrost.com/blog/post/atomic-web-design/)
- Internationalization (i18n)
- Site with a single page or multiple pages

# 🚀 Quick 

1. **Install nvm**

    Instructions [here](https://github.com/nvm-sh/nvm#installing-and-updating)

2. **Install and choose version node required by the project**

    Running the following commands `nvm` will take the node version written on the `.nvmrc` file
    ```shell
    nvm install && nvm use
    ```

3.  **Clone the project**

    You can use http or ssh

    ```shell
    git clone https://gitlab.com/disruptivo.io/internals/frontend/gatsby-base-template.git YOUR_SITE_FOLDER
    ```

4.  **Run the starter**

    Navigate into your new site’s directory and start it up

    ```shell
    cd YOUR_SITE_FOLDER
    npm i && npm start
    ```

5.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)

# 🧐 What's inside?

A quick look at the top-level files and directories you'll see in this starter

    .
    ├── node_modules
    ├── .vscode
    │   └── settings.json
    ├── assets
    │   ├── config
    │   │   └── site.js
    │   ├── content
    │   │   ├── organisms
    │   │   └── pages
    │   ├── images
    │   │   ├── 404
    │   │   ├── home
    │   │   └── favicon.png
    │   └── theme
    │       ├── border.scss
    │       ├── breakpoints.scss
    │       ├── colors.scss
    │       ├── font.js
    │       ├── icons.js
    │       └── spacing.scss
    ├── screenshots
    │   └── result.png
    ├── src
    │   ├── components
    │   │   ├── atoms
    │   │   ├── molecules
    │   │   ├── organisms
    │   │   ├── pages
    │   │   └── layouts
    │   ├── hooks
    │   │   └── useComponentsInViewPort.jsx
    │   │   └── useLoadLazyAnimationClass.jsx
    │   ├── pages
    │   │   └── 404.jsx
    │   ├── templates
    │   │   └── index.jsx
    │   ├── theme
    │   │   ├── first
    │   │   ├── main.scss
    │   │   └── typography.js
    │   └── utils
    │       ├── breakDownAllMarkdownNodes.js
    │       ├── fileNameToComponentName.js
    │       ├── getBaseUrl.js
    │       ├── removeLanguageCode.js
    │       ├── removeNumbersAndDashes.js
    │       ├── toLowerCaseFirstChart.js
    │       └── toUpperCaseFirstChart.js
    ├── .cz-config.js
    ├── .czrc
    ├── .eslintignore
    ├── .eslintrc.js
    ├── .gitignore
    ├── .huskyrc.js
    ├── .lintstagedrc
    ├── .nvmrc
    ├── Readme.md
    ├── extractor.php
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── package-lock.json
    ├── package.json
    └── prettier.config.js

- **`node_modules`**: This directory contains all of the modules of code that the project depends on (npm packages) are automatically installed

- **`.vscode`**: This directory contains configuration files for Visual Studio Code editor

- **`assets`**: This folder contains all customizable data:
   - `assets/config/site.js`: Configuration data of the site
   - `assets/content/*`: Data to show in the site, this folder uses [atomic design](https://bradfrost.com/blog/post/atomic-web-design/)
   - `assets/images/*`: Images folder
   - `assets/theme/*`: This contains data related to styles of the site

- **`screenshots`**: This directory contains some images for the `Readme.md` file

- **`src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”
  - `src/components`: This is a custom folder, which contains all components organized using [atomic design](https://bradfrost.com/blog/post/atomic-web-design/)
  - `hooks`: This folder will contains all the hooks (more information [here](https://es.reactjs.org/docs/hooks-intro.html)):
    - `useComponentsInViewPort.jsx`: This hooks returns the page intercepted by the viewholder, this uses the [Interception Observer](https://developer.mozilla.org/es/docs/Web/API/Intersection_Observer_API)
    - `useLoadLazyAnimationClass.jsx`: This hooks add the `.animation` class to the page intercepted by the viewholder, this uses the [Interception Observer](https://developer.mozilla.org/es/docs/Web/API/Intersection_Observer_API), so, in that way you can add animation when an page has the `.animation` class
  - `src/pages`: Files into this folder automatically become pages with paths based on their file name
  - `src/templates`: This folder contains templates for programmatically creating pages. Check out the [templates docs](https://www.gatsbyjs.org/docs/building-with-components/#page-template-components) for more detail
  - `assets/theme/*`: This contains data related to styles of the site, you can create a new folder if you want to create a new theme
  - `assets/utils/*`: This contains usefuls methods which are used in more than one component

- **`.cz-config.js`**: This file contains the configuration for using `cz-sutomizable`, you can find more information [here](https://github.com/leoforfree/cz-customizable#options)

- **`.czrc`**: This file contains the configuration for using `commitizen`, you can find more information [here](https://github.com/commitizen/cz-cli)

- **`.eslintignore`**: This file contains the folders and files that `eslint` will not analyze, you can find more information [here](https://eslint.org/docs/user-guide/configuring#eslintignore)

- **`.eslintrc.js`**: This file contains the configuration for using `eslint`, you can find more information [here](https://eslint.org/docs/user-guide/configuring)

- **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for

- **`.huskyrc.js`**: This file contains the configuration for using `husky`, you can find more information [here](https://github.com/typicode/husky#readme)

- **`.lintstagedrc`**: This file contains the configuration for using `lint-staged`, you can find more information [here](https://github.com/okonet/lint-staged)

- **`.nvmrc`**: This file contains the version of node used for building this starter, you can find more information [here](https://github.com/nvm-sh/nvm#nvmrc)

- **`Readme.md`**: A text file containing useful reference information about the project

- **`extractor.php`**: A php script for unzipping zip files, this is useful when you push your `web site` to a traditional hostings which use FTP protocol

- **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser

- **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail)

- **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process

- **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering

- **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly)**

- **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project

- **`prettier.config.js`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent

# 😎 FAQ

## **How to add a new section to my site?**

In this example we will add a Team section

**1. We need to create two files into folder `assets/content/pages` (why pages?, do not forget that we use [atomic design](https://bradfrost.com/blog/post/atomic-web-design/))**
   - `3-team.es.md`: Data for the Team section in Spanish
   - `3-team.en.md`: Data for the Team section in English

Example of data for adding to `3-team.es.md` and `3-team.en.md`
```md
---
team: {
  title: "Team",
  members: [
    {
      firstName: "Luis Angel",
      lastName: "Barrancos Ortiz",
      title: "Software Engineer",
      socialNetworks: [
        {
          iconName: "faTwitter",
          url: "https://twitter.com",
        },
        {
          iconName: "faFacebookF",
          url: "https://www.facebook.com",
        },
      ],
    },
    {
      firstName: "Julian Andres",
      lastName: "Gil Santos",
      title: "Software Engineer",
      socialNetworks: [
        {
          iconName: "faTwitter",
          url: "https://twitter.com",
        },
        {
          iconName: "faFacebookF",
          url: "https://www.facebook.com",
        },
      ],
    },
  ],
}
---
```

**2. We need to modify our Graphql schema in order to avoid problems with null data, which is located in `gatsby-node.js` file**

Example of data
```javascript
const typeDefs = [
  'type MarkdownRemark implements Node { frontmatter: Frontmatter }',
  `type Frontmatter {
    toolbar: Toolbar
    home: Home
    about: About
    team: Team
  }`,
  `type Toolbar {
    title: String
    buttonLabel: String
  }`,
  `type Home {
    title: String
    subTitle: String
    superTitle: String
    paragraph: String
    image: String
  }`,
  `type About {
    title: String
    paragraph: String
    iconNames: [String]
  }`,
  `type Team {
    title: String
    members: [Member]
  }`,
  `type Member {
    firstName: String
    lastName: String
    title: String
    socialNetworks: [SocialNetwork]
  }`,
  `type SocialNetwork {
    iconName: String
    url: String
  }`,
];
```

**3. We need to modify our Query in our Gatsby template `src/templates/index.jsx` (more information about Gatsby templates [here](https://www.gatsbyjs.org/docs/building-with-components/#page-template-components))**

Example of data
```javascript
export const query = graphql`
  query IndexQuery($langKey: String!) {
    site {
      siteMetadata {
        title
        description
        author
      }
    }
    allMarkdownRemark(
      filter: { fields: { langKey: { eq: $langKey } } }
      sort: { order: ASC, fields: [fields___directoryName, fields___fileName] }
    ) {
      nodes {
        frontmatter {
          toolbar {
            title
            buttonLabel
          }
          home {
            title
            subTitle
            superTitle
            paragraph
            image
          }
          about {
            title
            paragraph
            iconNames
          }
          team {
            title
            members {
              firstName
              lastName
              title
              socialNetworks {
                iconName
                url
              }
            }
          }
        }
        fields {
          fileName
          directoryName
        }
      }
    }
  }
`
```

**4. We need to create our page into `src/components/pages` folder and a molecule into `src/components/molecules` folder**

Team component (page)
```jsx
// Team.jsx
import React from 'react'
import propTypes from 'prop-types'
import HeaderAndBody from '../layouts/HeaderAndBody'
import Title from '../atoms/Title'
import Member from '../molecules/Member'

const Team = (
  {
    title,
    members,
  }
) => {

  const renderMembers = () => {

    return members.map(
      (
        {
          firstName,
          lastName,
          socialNetworks,
        }
      ) => {

        return (
          <Member
            key={firstName}
            firstName={firstName}
            lastName={lastName}
            socialNetworks={socialNetworks}
          />
        )

      }
    )

  }

  return (
    <div
      className='team'
    >
      <HeaderAndBody
        header={(
          <Title
            label={title}
          />
        )}
        body={(
          <div>
            {renderMembers()}
          </div>
        )}
      />
    </div>
  )

}

Team.propTypes = {
  title: propTypes.string.isRequired,
  members: propTypes.array.isRequired,
}

export default Team
```

Member component (molecule)
```jsx
// Member.jsx
import React from 'react'
import propTypes from 'prop-types'
import Paragraph from '../atoms/Paragraph'
import Icon from '../atoms/Icon'

const Member = (
  {
    firstName,
    lastName,
    socialNetworks,
  }
) => {

  const renderSocialNetworks = () => {

    return socialNetworks.map(
      (
        { iconName, url }
      ) => {

        return (
          <a
            key={url}
            href={url}
          >
            <Icon
              iconName={iconName}
            />
          </a>
        )

      }
    )

  }

  return (
    <div
      className='member'
    >
      <Paragraph
        label={`${firstName} ${lastName}`}
      />
      <div>
        {renderSocialNetworks()}
      </div>
    </div>
  )

}

Member.propTypes = {
  firstName: propTypes.string.isRequired,
  lastName: propTypes.string.isRequired,
  socialNetworks: propTypes.array.isRequired,
}

export default Member
```

**5. We must add the new page created to `src/components/pages/index.jsx` file**

Example of data
```javascript
// index.js
export { default as Home } from './Home'
export { default as About } from './About'
export { default as Team } from './Team'
```

**6. We must create the style for our Team component into `src/theme/first/pages` folder and another for our Member component into `src/theme/first/molecules`**

Team component style
```scss
// team.scss
.team {
  padding: $padding;
  margin-top: $margin * 2;
}
```

Member component style
```scss
// member.scss
.member {
  display: flex;
  flex-direction: column;
  align-items: center;
}

.member > span {
  margin: $margin;
}
```
**7. We must add the new styles created to `src/theme/main.scss` file**

Example of data
```scss
@import '../../assets/theme/colors.scss';
@import '../../assets/theme/border.scss';
@import '../../assets/theme/spacing.scss';
@import '../../assets/theme/breakpoints.scss';

@import './first/atoms/normalButton.scss';
@import './first/atoms/languageSelector.scss';

@import './first/molecules/toolbar.scss';
@import './first/molecules/member.scss';

@import './first/pages/home.scss';
@import './first/pages/about.scss';
@import './first/pages/team.scss';

@import './first/template/headerAndBody.scss';

@import './first/pages/404.scss';
```

**8. You have to load `http://localhost:8000` in order to see the section added**
![could not load the image](/screenshots/result.png)

## **How to add a more icons?**

You just have to import them using the `assets/theme/icons.js` file and then you can put the name of your icons in any Markdown file in order to render in any components using the `Icon` component, by the way, we are using [Fontawesome](https://fontawesome.com/icons?d=gallery)

If you want to use a icon which has not been imported, a question icon will be shown

## **How to add a more images?**

You just have to put your images in the `assets/images` folder and all the images are loaded automatically, then, you can use them using the `Image` component

## **Why markdown files located into `asssets/content/pages` folder have the following structure `number-page_name.language.md` (e.g. 1-home.en.md)?**

- The number is for moving the sections, so, if you want that the `about` section is located over the `home` section, you just have to swap the number of those sections

- The page name must be equal to your page which must be located in the `src/components/pages` folder

- The language is related to our internazionalization feature

## **How to add a more languages for our content?**

1. You must add your new language to `langTextMap` object into `assets/config/site.js` file, and if you are running your dev server, you have to stop it, remove the `.cache` folder and run again

Example of data
```javascript
module.exports = {
  title: 'Brand website with one view',
  description: 'Kick off your next, great Gatsby project with this great starter :)',
  author: 'luisbar',
  defaultLang: 'es',
  langTextMap: {
    es: 'Español',
    en: 'English',
    it: 'Italiano',
  },
}
```

2. You must have to create a new file for each page, in this case using the `it` word (e.g. 1-home.it.md)

## **How to change the font?**

For customizing the font we use [typography](https://kyleamathews.github.io/typography.js/), basically you have to change the `assets/theme/font.js` file

## **How to change colors?**

For changing the color you have to update the `assets/theme/colors.scss` file

## **What breakpoints.scss file works for?**

CSS breakpoints are points where the website content responds according to the device width, allowing you to show the best possible layout to the user, you can find more information [here](https://getflywheel.com/layout/css-breakpoints-responsive-design-how-to/), so, that file works for updating the breakpoints

## **What border.scss file works for?**

In this file you can save all related to borders, in order to have homogeneous borders

## **What spacing.scss file works for?**

In this file you can save all related to margin and padding, in order to have homogeneous spacing between components, for example, [Grommet uses three sizes](https://v1.grommet.io/docs/padding/)

## **How to use commitizen?**

You just have to run the command `npm run commit` instead of `git commit` and that is it

## **Why do you have a `layouts` folder instead of `templates` folder as suggest atomic design?**

Because Gatsby has [templates](https://www.gatsbyjs.org/docs/building-with-components/#page-template-components)

## **How to enable a site with multiple pages?**

- Go to config file located on `assets/config/site.js` and set the `multiplePages` flag to `true`
- Clean the cache `npm run clean`
- Run the project `npm start`

## **Do you have a brief explanation about each concept of atomic design?**

- atom: if atoms are the basic building blocks of matter, then the atoms of our interfaces serve as the foundational building blocks that comprise all our user interfaces. These atoms include basic HTML elements like form labels, inputs, buttons, and others that can’t be broken down any further without ceasing to be functional
- molecules: molecules are relatively simple groups of UI elements functioning together as a unit. For example, a form label, search input, and button can join together to create a search form molecule
- organism: organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms. These organisms form distinct sections of an interface
- templates: templates are page-level objects that place components into a layout and articulate the design’s underlying content structure
- pages: pages are specific instances of templates that show what a UI looks like with real representative content in place